# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kitemviews
pkgver=5.109.0
pkgrel=0
pkgdesc="Widget addons for Qt Model/View"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="GPL-2.0-only AND LGPL-2.1-only"
depends_dev="qt5-qtbase-dev"
makedepends="
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kitemviews.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kitemviews-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kitemviews.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
42aba62032fe145e5af1f3333ba2ba7ceb766c32591dbcef0c684f3b7eb1cb1f86506a4df8c8c84001ba168db2a2c326d8bb79da119c929c300fe86c255a6f4f  kitemviews-5.109.0.tar.xz
"

# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=syft
pkgver=0.87.1
pkgrel=0
pkgdesc="Generate a Software Bill of Materials (SBOM) from container images and filesystems"
url="https://github.com/anchore/syft"
license="Apache-2.0"
arch="all !armhf !armv7 !x86 !ppc64le !riscv64" # FTBFS on 32-bit arches, riscv64, ppc64le
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/anchore/syft/archive/v$pkgver/syft-$pkgver.tar.gz"
options="!check" # tests need docker

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "
		-X github.com/anchore/syft/internal/version.version=$pkgver
		" \
		-o bin/syft ./cmd/syft

	bin/syft completion bash > $pkgname.bash
	bin/syft completion fish > $pkgname.fish
	bin/syft completion zsh > $pkgname.zsh
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/syft -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
421ab69d531ea5bf63e4c7dc25463cd5e2aaf068e05d343174b71abd87478ca540ed2f14fff618ac06e585af32f19224f1e4225a09d34f4f90c23ffc8f67c809  syft-0.87.1.tar.gz
"

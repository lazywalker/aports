# Maintainer:
pkgname=py3-rpds-py
pkgver=0.9.2
pkgrel=2
pkgdesc="Python bindings to the Rust rpds crate"
url="https://github.com/crate-py/rpds"
arch="all"
license="MIT"
makedepends="
	cargo
	py3-gpep517
	py3-maturin
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/crate-py/rpds/releases/download/v$pkgver/rpds_py-$pkgver.tar.gz"
builddir="$srcdir/rpds_py-$pkgver"
options="net"

replaces="py3-rpds"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--config-json '{"build-args": "--frozen"}' \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
5a3d1b4ca27cd9b73edf7c03c8def5350949a56df0a322e317a7d3e6e31933694fe21928218ebeb51074322dcde6e4f807283e0359f8f31b0ca6b69180be3260  rpds_py-0.9.2.tar.gz
"

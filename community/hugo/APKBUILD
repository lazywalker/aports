# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=hugo
pkgver=0.117.0
pkgrel=0
pkgdesc="Fast and flexible static site generator written in Go"
url="https://gohugo.io/"
license="Apache-2.0"
arch="all !ppc64le" # fails tests
makedepends="go"
checkdepends="npm py3-docutils tzdata"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/gohugoio/hugo/archive/v$pkgver/hugo-$pkgver.tar.gz
	skip-para-test.patch
	skip-image-test.patch
	fix-go-1.21-compat.patch
	unicode-14-unprintable.patch
	"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o bin/$pkgname --tags extended
	./bin/hugo gen man
	./bin/hugo completion bash > hugo.bash
	./bin/hugo completion fish > hugo.fish
	./bin/hugo completion zsh > hugo.zsh
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/$pkgname "$pkgdir"/usr/bin/$pkgname
	install -Dm644 man/*.1 -t "$pkgdir"/usr/share/man/man1

	install -Dm644 hugo.bash \
		"$pkgdir"/usr/share/bash-completion/completions/hugo
	install -Dm644 hugo.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/hugo.fish
	install -Dm644 hugo.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_hugo
}

sha512sums="
93c06e957a38314bd8814e6f2a68f94ca082675ab5fc3cf7a07f79c55cee87dacdb1f49b01dc550dcd4eae3fb57c677e4411225ad37e0ec90a53c1b98798b683  hugo-0.117.0.tar.gz
6ba192d8cb67f115f7ce596c297a55fc64713a4cdb0077cfbb7e45051c7560f5b668da88f513d4f34d8e0eeb4a9d991c5312d62e454c85e95960d8a33f0f8f69  skip-para-test.patch
0feb0495f03315c9224b301ea955061fcbbf9ae8f871640fbb4b5c0792785a714f3a030f702a1f1f6a8048d60f07a133df4ced2a989df0b9c851b8503b59d26d  skip-image-test.patch
f7303624ee88fdcf3ef26a84bb32b791c9187f714c12517ff338b0152f39f5556f5c665209dfa43cd6c6f31d75495941a940303e5f26037c8342fa4e27c488ae  fix-go-1.21-compat.patch
c0b3e0f294c18671bd3ced5da069bee334691f53ebda0551aeb90c8694cb030292bc0050ba4e1818cba2657951901f2aeb3e0d5753ea21d9a7a0441b09684fd4  unicode-14-unprintable.patch
"

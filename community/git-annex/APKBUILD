# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=git-annex
pkgver=10.20230802
_bloomfilterver=2.0.1.0
pkgrel=0
pkgdesc="Manage files with git, without checking their contents into git"
url="http://git-annex.branchable.com"
arch="x86_64 aarch64" # limited by ghc
license="AGPL-3.0-or-later"
options="net"
depends="
	curl
	git
	rsync
	"
makedepends="
	alex
	cabal
	dbus-dev
	file-dev
	ghc
	gmp-dev
	gnutls-dev
	happy
	libffi-dev
	libgsasl-dev
	libxml2-dev
	ncurses-dev
	zlib-dev
	"
source="
	https://git.joeyh.name/index.cgi/git-annex.git/snapshot/git-annex-$pkgver.tar.gz
	https://hackage.haskell.org/package/bloomfilter-$_bloomfilterver/bloomfilter-$_bloomfilterver.tar.gz
	git-annex.config
	git-annex-block-crypton.patch
	bloomfilter-ghc9.2.diff
	fix-makefile.patch
	"
subpackages="$pkgname-doc $pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion"

# Add / remove '-' between "-f" and "FeatureName" to adjust feature build
_feature_flags="
	-fAssistant \
	-fWebApp \
	-fPairing \
	-fProduction \
	-fTorrentParser \
	-fMagicMime \
	-fBenchmark \
	-f-DebugLocks \
	-fDbus \
	-fNetworkBSD \
	-fGitLfs \
	-fHttpClientRestricted \
	"
_cabal_home="$srcdir/dist"
_cabal_config="git-annex"

cabal_update() {
	default_prepare
	msg "Freezing $pkgname dependencies"

	msg "Installing dependency bloomfilter-$_bloomfilterver out-of-tree"
	cd "$srcdir"/bloomfilter-*
	patch -p1 -i "$srcdir"/bloomfilter-ghc9.2.diff
	HOME="$_cabal_home" cabal v1-install

	# Resolve deps and generate fresh cabal.config with version constraints.
	HOME="$_cabal_home" cabal update
	for i in $_cabal_config; do
		cd "$srcdir/$i"*
		HOME="$_cabal_home" cabal v1-freeze --shadow-installed-packages

		# Add version tag at the first line.
		sed -i "1i--$pkgver" "cabal.config"

		mv "cabal.config" "$startdir/$i.config"
		cd "$startdir"
	done

	if ! abuild checksum; then
		die "Failed to update checksum, run 'abuild checksum' manually"
	fi
}

prepare() {
	default_prepare

	if [ "$(head -n 1 "$srcdir/git-annex.config")" != "--$pkgver" ]; then
		die "Requirements file is outdated, run 'abuild cabal_update'"
	fi

	for i in $_cabal_config; do
		cd "$srcdir"/$i*
		ln -sf "$srcdir/$i.config" cabal.config
	done

	# ghc version path
	export PATH="$PATH:/usr/lib/llvm14/bin"

	# problematic depend install
	HOME="$_cabal_home" cabal update

	msg "Installing dependency bloomfilter-$_bloomfilterver out-of-tree"
	cd "$srcdir"/bloomfilter-*
	patch -p1 -i "$srcdir"/bloomfilter-ghc9.2.diff
	HOME="$_cabal_home" cabal v1-install
}

build() {
	# ghc version path
	export PATH="$PATH:/usr/lib/llvm14/bin"

	msg "Building git-annex-$pkgver"
	HOME="$_cabal_home" cabal update
	HOME="$_cabal_home" cabal v1-install \
		--only-dependencies \
		--allow-newer=feed:base-compat \
		$_feature_flags
	HOME="$_cabal_home" cabal v1-configure $_feature_flags
	HOME="$_cabal_home" cabal v1-build -j
	mv dist/build/git-annex/git-annex .
	ln -s git-annex git-annex-shell
}

check() {
	"$builddir"/git-annex test
}

package() {
	HOME="$_cabal_home" make DESTDIR="$pkgdir" install
}

sha512sums="
1cf8626ba58869ecb7cd2965660e30e8e56590e19470a87f91b0c326b9d19478b7265663c7a03cf5f4785361429744827d4ef90718431a59cb2ede9f94ee7b68  git-annex-10.20230802.tar.gz
1031cc28c5e5c1b7687355b709d436ebab1956ffd7591a010fa3852a1bc7412925a365f99937feeacf4b1a5d3c7b34bc0370707ad6e4533280dcac4ea3fa7c21  bloomfilter-2.0.1.0.tar.gz
820acc3c8386dc5553af3cff3930c0c04d2195732c776d519b67f938b0c34facbb81b6542b906567dd67aad8fdd4a5ac7d82f2ae8cc0fd52b02a338597367f12  git-annex.config
700e561b79e186839dfb8c63b185ce5f83745f8c43473e0fd99df3cdae2165073a8fe4b1cfb2798b3ad852e22e3eea84a05b884ef10b89f80f9881e0921aab2f  git-annex-block-crypton.patch
500ebfd84e5771ed62ad2491b3f484d9a049091cd9bced072f34e64d81fd4db8aff2cfd9a0b2278335ac86198bfca551aec6ce3a2bd07d4484393a4288d3af35  bloomfilter-ghc9.2.diff
9300f883746d8726f47be6d194b5ac9550e2894669097f3202eca944221665bd3087a81b3f97b21f013eccaa6b5b1fb050e253ac18999c136db20056fadf2ed8  fix-makefile.patch
"

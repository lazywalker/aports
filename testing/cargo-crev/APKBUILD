# Contributor: lauren n. liberda <lauren@selfisekai.rocks>
# Maintainer: lauren n. liberda <lauren@selfisekai.rocks>
pkgname=cargo-crev
pkgver=0.24.3
pkgrel=0
pkgdesc="Cryptographically verifiable code review system for cargo"
url="https://github.com/crev-dev/cargo-crev"
# s390x: failing tests
arch="all !s390x"
license="MPL-2.0 OR MIT OR Apache-2.0"
makedepends="cargo cargo-auditable openssl-dev"
source="
	https://github.com/crev-dev/cargo-crev/archive/v$pkgver/cargo-crev-$pkgver.tar.gz
	our-openssl.patch
"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release -p cargo-crev
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/cargo-crev "$pkgdir"/usr/bin/cargo-crev
}

sha512sums="
b33361feea0f01e93de2eb38ec00685d0573e3150d5d2908fafeb0ebbbcd6df5e1cd33b8ba089a54955d5517f56df2a3643b90545c74bce13e8789fef5358621  cargo-crev-0.24.3.tar.gz
21d261efb7551bf395534d00d94fa8cd308b8f10e06b354688ba4bf4a540fcc762c3d8c9beafc972a11d3295f5159577d100696e2fc73941aca793244619ec55  our-openssl.patch
"

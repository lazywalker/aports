# Contributor: lauren n. liberda <lauren@selfisekai.rocks>
# Maintainer: lauren n. liberda <lauren@selfisekai.rocks>
pkgname=py3-flask-limiter
pkgver=3.4.0
pkgrel=0
pkgdesc="Rate Limiting extension for Flask"
url="https://github.com/alisaifee/flask-limiter"
arch="noarch"
license="MIT"
depends="
	py3-flask
	py3-limits
	py3-ordered-set
	py3-rich
	python3
	"
makedepends="py3-setuptools"
checkdepends="
	py3-flask-restful
	py3-flask-restx
	py3-limits-mongodb
	py3-limits-redis
	py3-pytest
	py3-pytest-cov
	"
options="!check"	# tests depend on unpackaged modules
subpackages="$pkgname-pyc"
source="
	https://github.com/alisaifee/flask-limiter/archive/refs/tags/$pkgver/flask-limiter-$pkgver.tar.gz

	our-std-is-good-enough.patch
	"
builddir="$srcdir/flask-limiter-$pkgver"

build() {
	python3 setup.py build
}

check() {
	pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
7504f8b7921bedc4845550a8f2f6b23b21d7b1af8ca63f901e60b1d861c459dcd2e2c2c4712852a4483b11b6906058f4059dafa2d221654acd0d396c7d44a4e5  flask-limiter-3.4.0.tar.gz
1b90e9134076cda249695d5ea741db9d205a2ae452c7d6edfe01eb37a221ce6f64b0e8ddcdbbee9b0e0fb16a28e5eabf14f1c1e41e965c7e3b93ea4f42caf553  our-std-is-good-enough.patch
"

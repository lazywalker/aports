# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=ccache
pkgver=4.8.2
pkgrel=1
pkgdesc="fast C/C++ compiler cache"
url="https://ccache.dev/"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	asciidoctor
	cmake
	hiredis-dev
	linux-headers
	perl
	samurai
	xxhash-dev
	zstd-dev
	"
checkdepends="bash util-linux-misc python3 redis"
subpackages="$pkgname-doc"
source="https://github.com/ccache/ccache/releases/download/v$pkgver/ccache-$pkgver.tar.xz
	ioctl.patch
	system-xxhash.patch
	"

build() {
	cmake -B build -G Ninja \
		-DCCACHE_DEV_MODE=OFF \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	ctest --output-on-failure --test-dir build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	local link=
	mkdir -p "$pkgdir"/usr/lib/ccache/bin

	for link in cc gcc g++ cpp c++ $CHOST-cc $CHOST-gcc \
		$CHOST-g++ $CHOST-c++ c89 c99; do
		ln -sf ../../../bin/ccache "$pkgdir"/usr/lib/ccache/bin/$link
	done
}

sha512sums="
2edf8da79a9286eb4ed5b6dc2063b55fb97e22a40d1c501c6f17b2f0ce32dfc02e99754afbabbd24fed8312e13e5b8bfc20687d0b5c0cf19fbf64e3adc7bb25b  ccache-4.8.2.tar.xz
785ce34305a3bb6c24117341157356c2bd2272eca9d58fa20acd14a023abe6f784c88d9c55656d2f37320392bb73a61c52cd8b6bd9ac7c5316c8ed187dd6f5fb  ioctl.patch
82b0b32237b2057bd7729e61e8f4e4716e0f1ab0b47b3eda37c07f673f5a1cd04bdf192946c06eb4387a0f6b6c207627f159969b05fdb642ebac6323e6ff5e3e  system-xxhash.patch
"
